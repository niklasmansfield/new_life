"""
NEW_world.py


"""


import random


class World(object):
	def __init__(self, cols=20, rows=10, default=False, pattern=["  ***", " *  *", "*   *"], chance=0.3):
		
		self.num_rows = rows
		self.num_cols = cols

		self.cells = [[default for c in range(cols)] for r in range(rows)]

		if pattern:
			pattern_rows = len(pattern)
			pattern_cols = 0
			for i in pattern:
				if len(i) > pattern_cols:
					pattern_cols = len(i)

			start_x = int(cols/2 - pattern_cols/2)
			start_y = int(rows/2 - pattern_rows/2)

			for y in range(pattern_rows):
				for x in range(pattern_cols):
					if not pattern[y][x] == ' ':
						self.alive(start_x + x, start_y + y)

		else:
			for x in range(self.num_cols):
				for y in range(self.num_rows):
					if random.randint(1, int(round(1/chance))) == 1: # This doesn't work well for probabilities > 0.5
						self.alive(x, y)



	def get_num_rows(self):
		return self.num_rows


	def get_num_columns(self):
		return self.num_cols


	def is_alive(self, col, row):
		return self.cells[row][col]


	def alive(self, col, row):
		self.cells[row][col] = True


	def dead(self, col, row):
		self.cells[row][col] = False





