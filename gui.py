"""
gui.py



"""

import curses
from world import World


class conwayGUI:
	def __init__(self, stdscreen):
		stdscreen.clear()
		curses.curs_set(0)
		stdscreen.border()
		stdscreen.refresh()

		self.window_size_y, self.window_size_x = stdscreen.getmaxyx()
		self.window_size_y -= 2 # Räknar bort ramen från spelområdet
		self.window_size_x -= 2 # Räknar bort ramen från spelområdet

		self.window = stdscreen


	def get_window_x(self):
		return self.window_size_x

	def get_window_y(self):
		return self.window_size_y


	def update_game(self, world=World()):
		"""
		updates the screen between game rounds
		"""

		self.window.clear()
		self.window.border()
		for y in range(world.num_rows):
			for x in range(world.num_cols):
				if world.is_alive(x, y):
					self.window.addstr(y+1, x+1, '*')
		self.window.addstr(self.window_size_y+1, self.window_size_x - 26, 'Push any button to continue')
		self.window.refresh()
		self.window.getch()


