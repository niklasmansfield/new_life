"""
life.py


"""

import curses
import gui
from world import World
# from lifetest import life



#@life(1)
def create_world(columns, rows):
	return World(columns, rows)


#@life(2)
def stay_alive(life_present, neighbours):
	if life_present:
		if neighbours == 2 or neighbours == 3:
			return True
		else:
			return False
	else:
		return False


#@life(3)
def birth(life_present, neighbours):
	if (not life_present) and neighbours == 3:
		return True
	else:
		return False


#@life(4)
def is_inhabited(world, column, row):
	if coordinates_exist(world, column, row):
		return world.is_alive(column, row)
	else:
		return False


def coordinates_exist(world, column, row):
	if column >= 0 and column < world.get_num_columns() and row >= 0 and row < world.get_num_rows():
		return True
	else:
		return False


#@life(5)
def count_living_neighbours(world, column, row):
	neighbours_list = [(-1,-1), ( 0,-1), ( 1,-1),
					   (-1, 0),          ( 1, 0),
					   (-1, 1), ( 0, 1), ( 1, 1)]
	living_neighbours = 0
	
	for (x, y) in neighbours_list:
		if is_inhabited(world, column + x, row + y):
			living_neighbours += 1

	return living_neighbours


#@life(6)
def live_die_or_be_born(world, column, row, num_neighbours):
	if stay_alive(is_inhabited(world, column, row), num_neighbours):
		return True
	elif birth(is_inhabited(world, column, row), num_neighbours):
		return True
	else:
		return False


#@life(7)
def update_world(world):
	new_world = create_world(world.get_num_columns(), world.get_num_rows())
	
	for x in range(world.get_num_columns()):
		for y in range(world.get_num_rows()):
			num_neighbours = count_living_neighbours(world, x, y)
			if live_die_or_be_born(world, x, y, num_neighbours):
				new_world.alive(x, y)
			else:
				new_world.dead(x, y)
	
	return new_world




def main(stdscreen):

	terminal_gui = gui.conwayGUI(stdscreen)
	# gui.update_game()

	my_world = create_world(terminal_gui.get_window_x(), terminal_gui.get_window_y())

	while True:
		terminal_gui.update_game(my_world)
		new_world = update_world(my_world)
		my_world = new_world

curses.wrapper(main)